from django.urls import path

from menu.views import DrinkTypeDetailView, DrinkTypeListView, DrinkTypeCreateView, DrinkTypeUpdateView, DrinkTypeDeleteView

app_name = "menu"


urlpatterns = [
    path(
        "types/",
        DrinkTypeListView.as_view(),
        name="types_list"
    ),
    path(
        "type/<int:pk>/",
         DrinkTypeDetailView.as_view(),
         name="type_detail"
    ),
    path(
        "new_type/",
        DrinkTypeCreateView.as_view(),
        name="new_type"
    ),
    path(
        "type/<int:pk>/update/",
        DrinkTypeUpdateView.as_view(),
        name="edit_type"
    ),
    path(
    "type/<int:pk>/delete/",
    DrinkTypeDeleteView.as_view(),
    name="delete_type"
    ),
]