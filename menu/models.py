from django.db import models

# Create your models here.

class DrinkType(models.Model):

    name = models.CharField(max_length=255)
    description = models.TextField()


class Drink(models.Model):

    name = models.CharField(max_length=255)
    description = models.TextField()

    typ = models.ForeignKey(DrinkType, on_delete=models.PROTECT, related_name="drinks")
