from django.shortcuts import render

# Create your views here.
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView
from django.utils.decorators import method_decorator
from menu.models import DrinkType

__all__ = [
    "DrinkTypeCreateView",
    "DrinkTypeDelete.View"
    "DrinkTypeDetailView",
    "DrinkTypeListView",
    "DrinkTypeUpdateView",
]


class DrinkTypeListView(ListView):
    model = DrinkType


class DrinkTypeDetailView(DetailView):
    model = DrinkType


@method_decorator(login_required, name="dispatch")
class DrinkTypeCreateView(CreateView):
    model = DrinkType
    fields = ("name", "description")

    success_url = reverse_lazy("menu:types_list")


@method_decorator(login_required, name="dispatch")
class DrinkTypeUpdateView(UpdateView):
    model = DrinkType
    fields = ("name", "description")

    def get_success_url(self):
        return reverse_lazy(
            "menu:type_detail",
            kwargs={"pk": self.object.id}
        )

@method_decorator(login_required, name="dispatch")
class DrinkTypeDeleteView(DeleteView):
    model = DrinkType

    success_url = reverse_lazy('menu:types_list')

